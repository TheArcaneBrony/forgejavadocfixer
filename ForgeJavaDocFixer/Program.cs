﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;

namespace ForgeJavaDocFixer
{
    class Program
    {
        private static string output = "";
        private static string WorkDir = "forgewd" + Directory.GetDirectories(".").Length;
        private static int errorcount = 0, runIter = 0;
        static void Main(string[] args)
        {
            //Clone forge
            Log("Cloning Forge...");
            //RunProcess("git clone https://github.com/MinecraftForge/MinecraftForge " + WorkDir);
            WorkDir = "forgewd1";
            //Go into forge directory
            Directory.SetCurrentDirectory(WorkDir);
            //Run gradlew setup
            Log("Setting up Forge...");
            //RunProcess(".\\gradlew setup");
            //Run gradlew javadoc to get list of errors
            Log("Running initial javadoc");
            RunProcess(".\\gradlew javadoc");
            //Check logs for error
            while(errorcount > 0 && runIter == 0)
            {
                Log($"Found {errorcount} errors!");
                CheckLogs();

                Log("Running javadoc #" + ++runIter);
                RunProcess(".\\gradlew javadoc");
            }
            
            Log("Process exited");
        }
        static void CheckLogs()
        {
            string[] lines = output.Split('\n');
            foreach(string line in lines)
            {
                if (line.Contains("warning"))
                {

                }
                if (line.Contains("error"))
                {
                    //Log(line);
                    ParseAndFixLine(line);
                }
            }
        }
        static void ParseAndFixLine(string line)
        {
            if (line.Contains("error"))
            {
                //
                if(line.Contains("self-closing element not allowed"))
                {
                    Log(line);
                    Debug.WriteLine(line.IndexOf(".java:" + 5));
                    string path = line.Substring(0,line.IndexOf(".java:")+5);
                    File.WriteAllText(path,File.ReadAllText(path).Replace("<br/>","<br>"));
                    Log(path);
                }
            }
        }
        //handle output (see RunProcess)
        static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (outLine.Data == null) return;
            //ConsoleColor bg = Console.BackgroundColor;
            if (outLine.Data.Contains("error: "))
            {
                Console.BackgroundColor = ConsoleColor.DarkRed;
                errorcount++;
            }
            if (outLine.Data.Contains("warning: ")) Console.BackgroundColor = ConsoleColor.DarkYellow;
            Log(outLine.Data);
            output += outLine.Data + "\n";
            Console.ResetColor();
        }
        //Execute arbitrary process
        static void RunProcess(string commandline)
        {
            output = "";
            ProcessStartInfo psi = new ProcessStartInfo("cmd ", "/c " + commandline)
            {
                RedirectStandardError = true,
                RedirectStandardOutput = true
            };
            Process proc = new Process();
            proc.StartInfo = psi;

            proc.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            proc.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);
            proc.Start();
            proc.BeginOutputReadLine();
            proc.BeginErrorReadLine();
            proc.WaitForExit();
        }
        //print to console, formatted
        static void Log(string text)
        {
            Console.WriteLine($"[{DateTime.Now.TimeOfDay}] {text}");
        }
    }
}
